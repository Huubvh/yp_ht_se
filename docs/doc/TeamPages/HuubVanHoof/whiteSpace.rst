.. _whitespace_intro:

===================
White Space Tutorial Jinja
===================
:author: Huub van Hoof
:date: 10 Dec 2020

.. toctree::
   :glob:
   :maxdepth: 2

Introduction
------------
In Jinja it is possible to generate python files from templates.
The problem with these templates is that they become hard to understand.
For example, rendering this simple class with two functions::

    class Foo:
        def test(self,age: int, name: str, ):
            print("hi " + name)

        def win(self,score: int, target, ) -> bool:
            return score > target

needs a template that looks like this::

    class Foo:
    {%- for func, func_data in func_list.items(): %}
        def {{func_data.name}}(self, 
    {%-     for parm, type in func_data.params.items(): %}
                {{- parm -}}
    {%          if type: %}: {{ type }}
    {%-         endif %}
                {{- ", " }}
    {%-     endfor -%}
            {{- ")" }}
    {%-     if func_data.typeHint: -%}
            {{- " -> " }}{{ func_data.typeHint }}
    {%-     endif %}
            {{- ":" }}
            {{ func_data.body }}
    {%-     block func_newline %}{{"\n"}}{% endblock %}
    {%- endfor %}
    {%- block class_newline %}{{"\n\n"}}{% endblock %}

As you can see, it is not immediatly clear what the intent of this template is.
The first for-loop makes sure that each function in the dictonary gets rendered::

    {%- for func, func_data in func_list.items(): %}

Next, the header of the function is generated. This is the most complicated part of the template.
The first line writes the name of the function and the opening braces::

    def {{func_data.name}}(self, 

After that, there is a for-loop to print the function arguments and their type hint(if any)::

    {%-     for parm, type in func_data.params.items(): %}
                {{- parm -}}
    {%          if type: %}: {{ type }}
    {%-         endif %}
                {{- ", " }}
    {%-     endfor -%}

Finally, the closing braces and the return-type for the function are specified by::

            {{- ")" }}
    {%-     if func_data.typeHint: -%}
            {{- " -> " }}{{ func_data.typeHint }}
    {%-     endif %}
            {{- ":" }}

A more convient way of doing this is shown in: :ref:'cleanClassTemplate'