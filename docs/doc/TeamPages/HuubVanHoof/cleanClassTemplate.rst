.. cleanclasstemplate:

===================
Clean Class Template Jinja
===================
:author: Huub van Hoof
:date: 23 Dec 2020

Introduction
------------
The goal of this assignment is to come up with a way of writing clean python Jinja templates.
The way that it is implemented here is by using macros.
The final output of the template should look like this::

    class Foo:
        def test(self,age: int, name: str):
            print("hi " + name)

        def win(self,score: int, target) -> bool:
            return score > target

The tricky part here is constructing the function header. To solve this a macro is used that handles the creation of the function header.
The template to construct the code shown above looks like::

    {% import "macro_functionheader.py.j2" as generator -%}
    {%- for className, func_list in data.items() -%}
        class {{ className }}:
        {%- for func, func_data in func_list.items() %}
            {{generator.function_header(func_data) | indent}}
                {{ func_data.body | indent | indent }}
        {%-     block func_newline %}{{"\n"}}{% endblock %}
        {%- endfor %}
        {%- block class_newline %}{{"\n"-}}{% endblock %}
    {%- endfor -%}

As you can see, the ''function_header'' macro is used. This macro is defined as::

    {%- macro function_header(func_data) -%}
    {%- set arg_string = func_args(func_data.vars) -%}
    {%- set typehint_string = func_typehint(func_data.typeHint) -%}
    def {{func_data.name}}({{arg_string}}){{typehint_string}}:
    {%- endmacro -%}

Here, the arguments are processed using ''func_args'' and the typehint is handled in ''func_typehint''.
The results of these function calls are printed and the funciton header is created.
The ''func_args'' macro is given by::

    {%- macro func_args(func_args) %}
    {%- set paramlist = ["self"] %}
    {%- for parm, type in func_args.items(): %}
    {%-     set param_def = parse_arg(parm, type) %}
    {%-     set paramlist = paramlist.append( param_def ) %}
    {%- endfor -%}
    {{- paramlist | join(', ') -}}
    {%- endmacro -%}

This function loops through all the parameters in the dictonary and passes them to ''parse_arg'' to process the typehint of that argument.
When all the arguments are processed they are converted to a string and returned to the fnction header.
The ''parse_arg'' macro looks like this:: 

    {%- macro parse_arg(parm, type) %}
    {%- if type: %}
    {{-     [parm, ": ", type] | join() }}
    {%- else: %}
    {{-     parm }}
    {%- endif %}
    {%- endmacro -%}

Finally, there is the ''func_typehint'' macro which handles the return typehunt of the function::

    {%- macro func_typehint(typeHint) %}
    {%- if typeHint: -%}
    {{-     " -> " }}{{ typeHint }}
    {%- endif %}
    {%- endmacro -%}

All these macros help in building clean python templates using Jinja.
