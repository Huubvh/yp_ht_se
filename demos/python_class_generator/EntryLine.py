import tkinter as tk

FONT = ("Verdana", 12)

FRAMESTYLE = {"relief": tk.RIDGE,
              "borderwidth": 5,
              "width": 1000}

TYPES = ["", "int", "float", "bool", "str"]


class FieldFactory:
    def make(self, master, fieldType, label, pos):
        if fieldType == "entry":
            return EntryField(master, label, pos)
        elif fieldType == "vars":
            return VarField(master, label, pos)
        elif fieldType == "type":
            return DropdownField(master, label, pos)
        elif fieldType == "body":
            return TextField(master, label, pos)


class AbstractFieldLine:
    def __init__(self, master, label, pos):
        self._text = label
        self._frame = tk.Frame(master=master, **FRAMESTYLE)
        self._fields = self._setupFields()
        self._frame.grid(row=pos[0], column=pos[1], padx=5, pady=5,
                         sticky='ew')

    def _setupFields(self):
        label = tk.Label(self._frame, text=self._text, font=FONT, width=15)
        label.grid(row=0, column=0, sticky='w')
        return self._addInputField()

    def emptyField(self):
        pass

    def _addInputField(self):
        pass

    def getFieldValue(self):
        pass


class EntryField(AbstractFieldLine):
    def _addInputField(self):
        entryField = tk.Entry(self._frame, font=FONT, width=30)
        entryField.grid(row=0, column=1, sticky='we')
        return entryField

    def getFieldValue(self):
        params = self._fields.get()
        return params

    def emptyField(self):
        self._fields.delete(0, tk.END)


class TextField(AbstractFieldLine):
    def _addInputField(self):
        entryField = tk.Text(self._frame, font=FONT, width=30, height=5)
        entryField.grid(row=0, column=1)
        return entryField

    def getFieldValue(self):
        params = self._fields.get("1.0", tk.END)
        return params.rstrip("\n")

    def emptyField(self):
        self._fields.delete("1.0", tk.END)


class VarField(AbstractFieldLine):
    variableInputRowIdx = 0
    varList = list()

    def _addInputField(self):
        self.varList.append(self._createInputField())
        self.variableInputRowIdx += 1
        return self.varList

    def _createInputField(self):
        nameField = tk.Entry(self._frame, font=FONT, width=13)
        nameField.grid(row=self.variableInputRowIdx, column=1)
        varType = tk.Label(self._frame, text="Type:")
        varType.grid(row=self.variableInputRowIdx, column=2)

        variable = tk.StringVar(self._frame)
        variable.set("Show selection")
        opt = tk.OptionMenu(self._frame, variable, *TYPES)
        opt.config(width=25, font=FONT)
        opt.grid(row=self.variableInputRowIdx, column=3)
        return (nameField, variable)

    def getFieldValue(self):
        var_dict = dict()
        for name, field in self._fields:
            if name.get() != "":
                varName = name.get()
                if field.get() == "Show selection":
                    varType = ""
                else:
                    varType = field.get()
                var_dict[varName] = varType
        return var_dict

    def emptyField(self):
        for name, field in self._fields:
            name.delete(0, tk.END)
            field.set("Show selection")


class DropdownField(AbstractFieldLine):
    def _addInputField(self):
        variable = tk.StringVar(self._frame)
        variable.set("Show selection")

        opt = tk.OptionMenu(self._frame, variable, *TYPES)
        opt.config(width=25, font=FONT)
        opt.grid(row=0, column=1)

        return variable

    def getFieldValue(self):
        if self._fields.get() == "Show selection":
            return ""
        return self._fields.get()

    def emptyField(self):
        self._fields.set("Show selection")
