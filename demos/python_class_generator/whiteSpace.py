from jinja2 import Environment, FileSystemLoader
import json


class TemplateRunner:
    def run(self):
        with open('jsontest.json') as json_file:
            data = json.load(json_file)

        templateLoader = FileSystemLoader(searchpath="./templates")
        templateEnv = Environment(loader=templateLoader)
        j2_template = templateEnv.get_template("pythonclasses.py.j2")
        new_template = j2_template.render(data=data)

        print(new_template)
        exec(new_template)
        # foo = Foo()
        # foo.test(25, "Huub")
        # print(f"have i won? {foo.win(6, 5)}")
        # print("test complete")
