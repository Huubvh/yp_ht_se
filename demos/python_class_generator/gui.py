import tkinter as tk
import json
from EntryLine import EntryField
from FunctionFrame import FunctionFrame
from whiteSpace import TemplateRunner


class MainWindow():
    def __init__(self):
        self._window = tk.Tk()
        self._window.title("Python Class Generator")
        self.counter = 0
        self._submitButton_Frame = tk.Frame(master=self._window)
        self._submitButton_Frame.columnconfigure(0, minsize=500)

        self._className_frame = tk.Frame(master=self._window)
        self._loadClassFrame()

        self._functionData_frame = FunctionFrame(self._window)

        self.submitButton = tk.Button(master=self._submitButton_Frame,
                                 text="Continue with functions", command=self.clicked)
        self.submitButton.grid(sticky=tk.E)
        self.saveButton = tk.Button(master=self._submitButton_Frame,
                               text="Save", command=self.click_save)
        self.addArgButton = tk.Button(master=self._submitButton_Frame,
                               text="add arg", command=self.add_argument)
        # Swap the order of `className_frame` and `functionData_frame`
        self._className_frame.pack(fill='x')
        self._submitButton_Frame.pack(side=tk.BOTTOM)
        self._container = dict()

        self._window.mainloop()

    def _loadClassFrame(self):
        self._className = EntryField(self._className_frame, "Class Name:", (0, 0))

    def clicked(self):
        if(self.counter):
            self.add_function()
        else:
            self._className_frame.pack_forget()
            self._functionData_frame.pack()
            self.submitButton.configure(text="Add more Functions")
            self.class_name = self._className.getFieldValue()
            self._container[self.class_name] = dict() 
            self.saveButton.grid(sticky=tk.W)
            self.addArgButton.grid(sticky=tk.EW)
            self.counter = 1

    def click_save(self):
        if(self._functionData_frame.getValues()):
            function_dict = self._functionData_frame.getValues()
            function_name = list(function_dict.keys())[0]
            self._container[self.class_name][function_name] = function_dict[function_name]

        with open("jsontest.json", "w") as write_file:
            json.dump(self._container, write_file, indent=4)

        self._window.destroy()
        templaterunner = TemplateRunner()
        templaterunner.run()

    def add_function(self):
        if(self._functionData_frame.getValues()):
            self.class_name = self._className.getFieldValue()
            function_dict = self._functionData_frame.getValues()
            function_name = list(function_dict.keys())[0]
            self._container[self.class_name][function_name] = function_dict[function_name]
        self._functionData_frame.emptyFields()

    def add_argument(self):
        self._functionData_frame._functionVariable._addInputField()


if __name__ == "__main__":
    mainwindow = MainWindow()
