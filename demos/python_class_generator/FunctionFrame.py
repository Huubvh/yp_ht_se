import tkinter as tk
from EntryLine import FieldFactory


class FunctionFrame:
    def __init__(self, master):
        self._functionData_frame = tk.Frame(master=master)
        self._fieldfactory = FieldFactory()
        self._loadFrame()

    def _loadFrame(self):
        self._functionName = self._fieldfactory.make(self._functionData_frame, 'entry', "Function:", (0, 0))
        self._functionVariable = self._fieldfactory.make(self._functionData_frame, 'vars', "Variable:", (1, 0))
        self._functionRetType = self._fieldfactory.make(self._functionData_frame, 'type', "Function type:", (2, 0))
        self._functionBody = self._fieldfactory.make(self._functionData_frame, 'body', "Body:", (3, 0))

    def pack(self):
        self._functionData_frame.pack(fill='x')

    def getValues(self) -> dict():
        funcName = self._functionName.getFieldValue()
        funcVars = self._functionVariable.getFieldValue()
        funcType = self._functionRetType.getFieldValue()
        funcBody = self._functionBody.getFieldValue()
        values = dict()
        values[funcName] = dict()
        values[funcName]['name'] = funcName
        values[funcName]['vars'] = funcVars
        values[funcName]['typeHint'] = funcType
        values[funcName]['body'] = funcBody
        if(funcName == "" or funcBody == ""):
            values = 0
        return values

    def emptyFields(self):
        self._functionName.emptyField()
        self._functionVariable.emptyField()
        self._functionRetType.emptyField()
        self._functionBody.emptyField()
